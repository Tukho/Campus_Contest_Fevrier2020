<?php

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">


    <link rel="icon" type="image/png" href="logos/logoMomo.ico" />

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-158829501-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-158829501-1');
    </script>


    <!--- Intégration JS --------------->

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <!--- Intégration Bootstrap -------->

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <!-- ---------------- FONTS ------------------------ -->

    <link href="https://fonts.googleapis.com/css?family=Permanent+Marker&display=swap" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Special+Elite&display=swap" rel="stylesheet">

    <!-- ----------------- CSS -------------------------- -->

    <link rel="stylesheet" href="css/presentation.css">

    <title>Présentation Morgan NOTT - Etudiant Développeur Web et Mobile</title>
</head>
<body id="your-element-selector">

<!-- -------- Navbar ------------- -->

<header>

    <nav class="mb-1 navbar navbar-expand-lg navbar-dark secondary-color lighten-2">

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-555"
                aria-controls="navbarSupportedContent-555" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent-555">

            <ul class="navbar-nav mr-auto">


                <li class="nav-item active">
                    <a class="nav-link" href="index.php">Accueil
                        <span class="sr-only">(current)</span>
                    </a>
                </li>


                <li class="nav-item">
                    <a class="nav-link" href="miniblog/index.html">Blog</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="documents/morgan_nott_cv.pdf" target="_blank">C.V.</a>
                </li>

                <li class="nav-item">
                    <a   class="nav-link" href="cv/cv.html">CV. en ligne</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="projets.php">Projets</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="recommandations.php">Recommandations</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="https://www.linkedin.com/in/morgan-nott" target="_blank">LinkedIn</a>
                </li>

                <li class="nav-item">
                    <a  class="nav-link" href="https://github.com/Ketzatl?tab=repositories" target="_blank">GitHub</a>
                </li>

            </ul>

            <ul class="navbar-nav ml-auto nav-flex-icons">


                <form class="form-inline" method="GET">
                    <input class="form-control mr-sm-2" type="search" placeholder="Coming Soon..." aria-label="Search">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit" value="Valider">Valider</button>
                </form>

            </ul>


        </div>
    </nav>

</header>

<main>
<div id="contPresent">

    <div class="div1 elem">
        <h2>Présentation</h2>
        <h1>Morgan NOTT</h1>
        <h1>Etudiant Développeur Concepteur</h1>
        <h1>Web & Mobile</h1>
    </div>

    <img class="elem" src="pictures/IMG_1187.jpg" >


    <h3 class="elem">Campus Academy Nantes</h3>

    <img id="logo" src="logos/campuslogo.png" style="padding: 1%"><br><br>


    <div class="div1 elem">
        <p>Dans le cadre d'une reconversion professionnelle, je suis un cursus BACHELOR Informatique, "Développeur Concepteur FullStack" en 3 années.</p>
        <p>La 1ère année se déroule dans les locaux de "Campus Academy", les deux années suivantes se dérouleront en alternance, dans le cadre d'un contrat de professionnalisation.
        </p><br><br>
        <img id="imgCA" src="pictures/campus.jpeg"
        <br><br>
        <p>Auparavant "Conseiller de Vente spécialisé" puis "Gestionnaire de Rayon" en Grand Magasin de Bricolage (Leroy Merlin, Mr.Bricolage), j'ai décidé de me reconvertir dans un domaine qui allie à merveille, passion et avenir.

        <br><br>Au travers de ces différentes expériences, j'ai pu aquérir et développer de nombreuses compétences (gestion d'équipe, relation client, rigueur, esprit d'équipe...).</p>


    </div>
</div>
</main>



<!-------------------------------- Footer ------------------------------------>


<footer class="page-footer font-small indigo">
    <div>
        <div class="container">


            <div id="container3" class="row py-4 d-flex align-items-center">

                <!-- Container -->

                <div id="contFooter"><br>

                    <div class="col-md-6 col-lg-5 text-center text-md-left mb-4 mb-md-0" >
                        <a href="https://www.linkedin.com/in/morgan-nott" target="_blank"><h6 class="mb-0" style="color: whitesmoke; font-family: 'Permanent Marker', cursive; font-size: 1.5em;">Rejoignez-moi sur LinkedIn !</h6></a>
                    </div>


                    <div align="center">
                        <a href="https://www.linkedin.com/in/morgan-nott" target="_blank"><img alt="logo LinkedIn" src="https://upload.wikimedia.org/wikipedia/commons/e/e9/Linkedin_icon.svg"  style="width: 4em"></a>
                    </div>

                </div>

            </div>

        </div>
    </div><br><br>
    <!-- Footer Links -->
    <div class="container">

        <!-- Grid row-->
        <div class="row text-center d-flex justify-content-center pt-5 mb-3">

            <!-- Grid column -->
            <div class="col-md-2 mb-3">
                <h6 class="text-uppercase font-weight-bold">
                    <a href="#!">Blog</a>
                </h6>
            </div>
            <!-- Grid column -->
            <div class="col-md-2 mb-3">
                <h6 class="text-uppercase font-weight-bold">
                    <a href="index.php">Accueil</a>
                </h6>
            </div>
            <!-- Grid column -->

            <!-- Grid column -->
            <div class="col-md-2 mb-3">
                <h6 class="text-uppercase font-weight-bold">
                    <a href="documents/morgan_nott_cv.pdf" target="_blank">C.V.</a>
                </h6>
            </div>
            <!-- Grid column -->

            <!-- Grid column -->
            <div class="col-md-2 mb-3">
                <h6 class="text-uppercase font-weight-bold">
                    <a href="cv/cv.html">CV en ligne</a>
                </h6>
            </div>
            <!-- Grid column -->

            <!-- Grid column -->
            <div class="col-md-2 mb-3">
                <h6 class="text-uppercase font-weight-bold">
                    <a href="projets.php">Projets</a>
                </h6>
            </div>
            <!-- Grid column -->

            <!-- Grid column -->
            <div class="col-md-2 mb-3">
                <h6 class="text-uppercase font-weight-bold">
                    <a href="recommandations.php">Recommandations</a>
                </h6>
            </div>
            <div class="col-md-2 mb-3">
                <h6 class="text-uppercase font-weight-bold">
                    <a href="https://www.linkedin.com/in/morgan-nott" target="_blank">LinkedIn</a>
                </h6>
            </div>
            <div class="col-md-2 mb-3">
                <h6 class="text-uppercase font-weight-bold">
                    <a href="contact.php">Contact</a>
                </h6>
            </div>
            <div class="col-md-2 mb-3">
                <h6 class="text-uppercase font-weight-bold">
                    <a href="https://github.com/Ketzatl?tab=repositories">GitHub</a>
                </h6>
            </div>




            <!-- Grid column -->

        </div>
        <!-- Grid row-->
        <hr class="rgba-white-light">






    </div>


    </div>

    <div class="footer-copyright text-center py-3" style="color: #EFD0EF">© 2020 Copyright
        <a href="https://mdbootstrap.com/education/bootstrap/"> Morgan-NOTT.fr</a>
    </div>

    <br>
</footer>





</script>

<script src="vanta-master/vendor/three.r95.min.js"></script>
<script src="vanta-master/dist/vanta.fog.min.js"></script>
<script>
    VANTA.FOG({
        el: "#your-element-selector",
        mouseControls: true,
        touchControls: true,
        minHeight: 200.00,
        minWidth: 200.00,
        highlightColor: 0x5b6570,
        midtoneColor: 0x520a84
    })
</script>
</body>
</html>
