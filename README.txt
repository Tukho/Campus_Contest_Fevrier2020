Projet Campus Contest

                            PortFolio - Morgan NOTT, Etudiant Bachelor 1A


    - Ce projet consiste en la création d'un portfolio professionnel. Nous devons y integrer notre CV et une version de ce CV en ligne.
Nous présenterons également nos différents projets réalisés au cours de cette année. Un formulaire de contact permettant à l'utilisateur de
laisser une recommandation et une base de donnée regroupant les informations de cet utilisateurs ainsi que son message,
devront être mis en place également.

    - Je ne sais pas encore si je la laisserai dans la version finale, mais je vais essayer de développer une partie blog/actualité.
Le contenu de la partie "Projet" sera mis à jour au fur et à mesure de l'avancée de ceux-ci,
 j'y rajouterai une partie "Stage" ou je détaillerai les technologies utilisées et les projets auxquels j'aurai participé.

    - J'aimerai integrer une fonction "recherche" dans la navBar (en haut à droite), qui permettrait à l'utilisateur de filtrer
    les projets par technologie utilisée. Ex : Tout les projets utilisant le php, ou javascript.

    - Une fois le site en ligne, je verrai comment réagissent les animations en fond d'écran, et les changerai si besoin.