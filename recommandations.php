<?php

// Connexion à la bdd //

$bdd = new PDO('mysql:host=localhost;dbname=portfolio_cards', 'root', 'root');

// Préparation de la requête //

$pdoStat = $bdd->prepare('SELECT * FROM membres');

// Execution de la requête //

$executeIsOk = $pdoStat->execute();

// Récupération des résultats //

$contacts = $pdoStat->fetchAll();

?>


<!DOCTYPE html>
<html lang="fr">
<head>
    <link rel="icon" type="image/png" href="logos/logoMomo.ico" />

    <meta charset="UTF-8">

    <!-- -------------------------- Appel de la police perso. --------------------------- -->

    <link href="https://fonts.googleapis.com/css?family=Pacifico|Squada+One&display=swap" rel="stylesheet">

    <!----------------------------------------------------------------------------------- -->

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-158829501-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-158829501-1');
    </script>


    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <link rel="stylesheet" href="css/recommandations.css">

    <title>Recommandations Morgan NOTT</title>
    
</head>

<body id="your-element-selector">

<!-- ------------------------------------ Navbar ------------------------------------------ -->


<header>

    <nav class="secondary-color mb-1 navbar navbar-expand-lg navbar-dark lighten-2">

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-555"
                aria-controls="navbarSupportedContent-555" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent-555">

            <ul class="navbar-nav mr-auto">

                <li class="nav-item active">
                    <a class="nav-link" href="index.php">Accueil
                        <span class="sr-only">(current)</span>
                    </a>
                </li>


                <li class="nav-item">
                    <a class="nav-link" href="documents/morgan_nott_cv.pdf" target="_blank">C.V.</a>
                </li>

                <li class="nav-item">
                    <a   class="nav-link" href="cv/cv.html">CV. en ligne</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="projets.php">Projets</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="recommandations.php">Recommandations</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="https://www.linkedin.com/in/morgan-nott" target="_blank" >LinkedIn</a>
                </li>

            </ul>

            <ul class="navbar-nav ml-auto nav-flex-icons">


                <form class="form-inline" method="GET">
                    <input class="form-control mr-sm-2" type="search" placeholder="Coming Soon..." aria-label="Search">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit" value="Valider">Valider</button>
                </form>

            </ul>


        </div>
    </nav>

</header>

<br><br>



    <div align="center">
        <img src="https://cdn.pixabay.com/photo/2016/11/22/21/26/notebook-1850613_1280.jpg" width="300em">
        <br><br>
        <a href="contact.php"><button type="button" class="btn btn-secondary">Me laisser une recommandation >></button></a>
    </div>

            <h1  class="ml15"  align="center" >Recommandations  </h1><br>
            <h1  class="ml15"  align="center" >Professionnelles</h1>

<br><br>

<section>

<?php foreach ($contacts as $contact): ?>

    <div id="reco" align="center">

            <div class="card w-50" align="center" class="rounded">
                <div class="card-header" align="center">

                    <h3 align="center"><?= $contact['prenom'] ?>  <?= $contact['nom'] ?></h3> <br>
                    <p style="font-weight: bold">Posté le : </p><p style="color: whitesmoke"><?= $contact['date'] ?></p>
                </div>

                <div class="card-body" align="center" style="background: #BFA6BF">

                    <h4 class="card-title" align="center">Entreprise : </h4><h5><?= $contact['entreprise'] ?></h5><h4>  Poste : </h4><h5><?= $contact['poste'] ?></h5> <br>

                    <h6 class="card-text" align="center" style="font-style: italic">Message : </h6><p> « <?= $contact['message'] ?> » </p>


                </div>

            </div>
        <br><br><br>
        <?php endforeach; ?>
    </div>

</section>

<!-------------------------------- Footer ------------------------------------>










<footer class="page-footer font-small indigo">
    <div style="background-color: ;">
        <div class="container">



        </div>
    </div><br><br>

    <div class="container">


        <div class="row text-center d-flex justify-content-center pt-5 mb-3">


            <div class="col-md-2 mb-3">
                <h6 class="text-uppercase font-weight-bold">
                    <a href="miniblog/index.html">Blog</a>
                </h6>
            </div>

            <div class="col-md-2 mb-3">
                <h6 class="text-uppercase font-weight-bold">
                    <a href="presentation.php">Présentation</a>
                </h6>
            </div>

            <div class="col-md-2 mb-3">
                <h6 class="text-uppercase font-weight-bold">
                    <a href="index.php">Accueil</a>
                </h6>
            </div>

            <div class="col-md-2 mb-3">
                <h6 class="text-uppercase font-weight-bold">
                    <a href="documents/morgan_nott_cv.pdf" target="_blank">C.V.</a>
                </h6>
            </div>

            <div class="col-md-2 mb-3">
                <h6 class="text-uppercase font-weight-bold">
                    <a href="cv/cv.html">CV en ligne</a>
                </h6>
            </div>

            <div class="col-md-2 mb-3">
                <h6 class="text-uppercase font-weight-bold">
                    <a href="projets.php">Projets</a>
                </h6>
            </div>

            <div class="col-md-2 mb-3">
                <h6 class="text-uppercase font-weight-bold">
                    <a href="https://www.linkedin.com/in/morgan-nott" target="_blank">LinkedIn</a>
                </h6>
            </div>

            <div class="col-md-2 mb-3">
                <h6 class="text-uppercase font-weight-bold">
                    <a href="https://github.com/Ketzatl?tab=repositories" target="_blank">GitHub</a>
                </h6>
            </div>

        </div>

        <hr class="rgba-white-light">






    </div>


    </div>

    <div class="footer-copyright text-center py-3" style="color: #EFD0EF">© 2020 Copyright:
        <a href="https://mdbootstrap.com/education/bootstrap/"> Morgan-NOTT.fr</a>
    </div>

    <br>
</footer>


<!-- ---------------------- Javascript --------------------------------------- -->


<script>
    anime.timeline({loop: true})
        .add({
            targets: '.ml15 .word',
            scale: [14,1],
            opacity: [0,1],
            easing: "easeOutCirc",
            duration: 800,
            delay: (el, i) => 800 * i
        }).add({
        targets: '.ml15',
        opacity: 0,
        duration: 1000,
        easing: "easeOutExpo",
        delay: 1000
    });
</script>

<script src="vanta-master/vendor/three.r95.min.js"></script>
<script src="vanta-master/dist/vanta.fog.min.js"></script>
<script>
    VANTA.FOG({
        el: "#your-element-selector",
        mouseControls: true,
        touchControls: true,
        minHeight: 200.00,
        minWidth: 200.00,
        highlightColor: 0x5b6570,
        midtoneColor: 0x520a84
    })
</script>

<!-- ---------------------------------------------------------------------------- -->


</body>

</html>


