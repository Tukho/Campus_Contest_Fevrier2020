




<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta  name="description" content="Morgan NOTT Développeur Web et Mobile - Etudiant Bachelor Informatique - En recherche d'Alterance pour Octobre 2020" >
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="icon" type="image/png" href="logos/logoMomo.ico" />

                                        <!--- Intégration JS --------------->

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

                                    <!--- Intégration Bootstrap -------->

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

                                 <!-- ---------------- FONTS ------------------------ -->

    <link href="https://fonts.googleapis.com/css?family=Permanent+Marker&display=swap" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Special+Elite&display=swap" rel="stylesheet">

                                 <!-- ----------------- CSS -------------------------- -->

    <link rel="stylesheet" href="css/default.css">


    <title>Morgan NOTT Développeur Web et Mobile</title>

</head>

<body id="your-element-selector">

                                        <!-- -------- Navbar ------------- -->

        <header>

            <nav class="mb-1 navbar navbar-expand-lg navbar-dark secondary-color lighten-2">

                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-555"
                        aria-controls="navbarSupportedContent-555" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent-555">

                    <ul class="navbar-nav mr-auto">

                        <li class="nav-item">
                            <a class="nav-link" href="miniblog/index.html">Blog</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="presentation.php">Présentation</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="documents/morgan_nott_cv.pdf" target="_blank">C.V.</a>
                        </li>

                        <li class="nav-item">
                            <a   class="nav-link" href="cv/cv.html">CV. en ligne</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="projets.php">Projets</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="recommandations.php">Recommandations</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="https://www.linkedin.com/in/morgan-nott" target="_blank">LinkedIn</a>
                        </li>

                        <li class="nav-item">
                            <a  class="nav-link" href="https://github.com/Ketzatl?tab=repositories" target="_blank">GitHub</a>
                        </li>

                    </ul>

                    <ul class="navbar-nav ml-auto nav-flex-icons">


                        <form class="form-inline" method="GET">
                            <input class="form-control mr-sm-2" type="search" placeholder="Coming Soon..." aria-label="Search">
                            <button class="btn btn-outline-success my-2 my-sm-0" type="submit" value="Valider">Valider</button>
                        </form>

                    </ul>


                </div>
            </nav>

        </header>

        <!-- --------------------------- Titre 1/2 ------------------------------------------ -->

    <br>
                                        <div class="container">
                                            <button type="button" class="btn btn-dark" data-toggle="modal" data-target="#cookieModal">
                                                See Cookies
                                            </button>
                                        </div>

                                        <div class="modal fade" id="cookieModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-body">
                                                        <div class="notice d-flex justify-content-between align-items-center">
                                                            <div class="cookie-text">Ce site utilise des cookies pour vous offrir le meilleur service. En poursuivant votre navigation, vous acceptez l’utilisation des cookies.</div>
                                                            <div class="buttons d-flex flex-column flex-lg-row">
                                                                <a href="#a" class="btn btn-success btn-sm" data-dismiss="modal">J'Accepte</a>
                                                                <a href="#a" class="btn btn-secondary btn-sm" data-dismiss="modal">En savoir plus !</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div><br>
    <div id="titre">

        <center>
        <h1 class="ml15" style="font-size: 2.5em" >
            <span class="word" style="color:whitesmoke" >Morgan</span>

            <span class="word" style="color:whitesmoke" >NOTT</span>
        </h1>
        </center>


        <!-- ---------------------------- Logo Campus Academy ---------------------------- -->

        <br>
        <center>
                <div>
                    <img  class="ml15" src="logos/campuslogo.png" width="15%">
                </div>
        </center>

        <!-- --------------------------- Titre 2/2 ------------------------------------------ -->

        <br>
        <center>
            <h1  >
                <span class="word">Développeur Concepteur</span>
                <br><br>
                <span class="word" style="color:whitesmoke">d'Applications</span>
                <br><br>
                <span class="word"> Web & Mobile Junior</span>

            </h1>
        </center>
        <br>
        <br>
    </div>

<main>

                <!-- ------------------- Citation Amel BENT ------------------------ -->

    <blockquote align="center">
        <span  style="font-family: 'Permanent Marker', cursive;
                    font-size: 200%;
                    color: whitesmoke" >
        « Viiiiiiisez la lune, ça ne m'fait pas peur !!! »</span><br>
        <cite style="font-weight: bold">- Amel BENT (2014 Après J.C.)</cite>
    </blockquote>
    <br>

                <!-- ------------------------ Vidéo Montage ------------------------- -->


<div align="center">
         <video autoplay loop controls src="videos/project_export.mp4" alt="vidéo Morgan NOTT Développeur web et mobile"></video>
</div>



    <br><br><br>
    <div>
        <h2 align="center" style="color: whitesmoke; font-size: 1.5em" class="ml15" >Mes Derniers Projets</h2>
    </div>


    <!--------------------------- Container (3 premiers Projets) ---------------------  -->


                        <!-- --------------- Projet 1 ------------------- -->

    <div class="container-fluid">
        <div class="row" id="menu">

            <article class="col-xs-12 col-sm-12 col-md-4 col-lg-4" align="center" >
                <div class="card-header">
                    <img class="card-img-top img-fluid"  style="border-radius: 20px" src="pictures/angularjs.jpg" alt="Projet 1 - Morgan NOTT Développeur web et mobile" width="250em" height="250em">
                </div>
                <div class="card-body">
                    <h3 class="card-title" style=" font-family: 'Permanent Marker', cursive;" >Projet 1</h3>
                    <p class="card-text" style="color: whitesmoke; font-family: 'Permanent Marker', cursive;">Création d’une Application Mobile : Fil d’actualités Inter-Campus</p>
                    <a href="Pages_Projets/projet1.php" class="btn btn-primary">Vers Projet 1</a>
                </div>
            </article>


                    <!-- --------------- Projet 2 ------------------- -->


            <article class="col-xs-12 col-sm-12 col-md-4 col-lg-4" align="center">
                <div class="card-header">
                    <img class="card-img-top img-fluid"  style="border-radius: 20px" src="https://cdn.pixabay.com/photo/2017/07/01/15/38/coming-soon-2461832_1280.jpg" alt="Projet Morgan NOTT Développeur web et mobile" width="250em" height="250em">
                </div>
                <div class="card-body">
                    <h3 class="card-title" style=" font-family: 'Permanent Marker', cursive;" >Projet 2</h3>
                    <p class="card-text" style="color: whitesmoke; font-family: 'Permanent Marker', cursive;">Création d’un jeu Mobile : Jeu Tower Defense Inter-Campus</p>
                    <a href="comingsoon_13/index.html" class="btn btn-primary">Vers Projet 2</a>
                </div>
            </article>


                     <!-- --------------- Projet 3 ------------------- -->


            <article class="col-xs-12 col-sm-12 col-md-4 col-lg-4" align="center">
                <div class="card-header">
                    <img class="card-img-top img-fluid" style="border-radius: 20px" src="https://cdn-istoedinheiro-ssl.akamaized.net/wp-content/uploads/sites/17/2019/07/aplicativos-2.jpg" alt="Recommandations Morgan NOTT Développeur web et mobile" width="250em" height="250em">
                </div>
                <div class="card-body">
                    <h3 class="card-title" style=" font-family: 'Permanent Marker', cursive;" >Projet 3</h3>
                    <p class="card-text" style="color: whitesmoke; font-family: 'Permanent Marker', cursive;">Création d’un réseau social inter-Campus sur Mobile & Web</p>
                    <a href="comingsoon_13/index.html" class="btn btn-primary">Vers Projet 3</a>
                </div>
            </article>
        </div>
    </div>

</main>
<br>
                <!-------------------------------- Footer ------------------------------------>










                                        <footer class="page-footer font-small indigo">
                                            <div style="background-color: ;">
                                                <div class="container">


                                                    <div id="container3" class="row py-4 d-flex align-items-center">

                                                        <div id="contFooter"><br>

                                                            <div class="col-md-6 col-lg-5 text-center text-md-left mb-4 mb-md-0" >
                                                                <a href="https://www.linkedin.com/in/morgan-nott" target="_blank"><h6 class="mb-0" style="color: whitesmoke; font-family: 'Permanent Marker', cursive; font-size: 1.5em;">Rejoignez-moi sur LinkedIn !</h6></a>
                                                            </div>


                                                            <div align="center">
                                                                <a href="https://www.linkedin.com/in/morgan-nott" target="_blank"><img alt="logo LinkedIn" src="https://upload.wikimedia.org/wikipedia/commons/e/e9/Linkedin_icon.svg"  style="width: 4em"></a>
                                                            </div>

                                                        </div>

                                                    </div>

                                                </div>
                                            </div><br><br>

                                            <div class="container">


                                                <div class="row text-center d-flex justify-content-center pt-5 mb-3">


                                                    <div class="col-md-2 mb-3">
                                                        <h6 class="text-uppercase font-weight-bold">
                                                            <a href="miniblog/index.html">Blog</a>
                                                        </h6>
                                                    </div>

<div class="col-md-2 mb-3">
                                                        <h6 class="text-uppercase font-weight-bold">
                                                            <a href="index.php">Accueil</a>
                                                        </h6>
                                                    </div>

                                                    <div class="col-md-2 mb-3">
                                                        <h6 class="text-uppercase font-weight-bold">
                                                            <a href="documents/morgan_nott_cv.pdf" target="_blank">C.V.</a>
                                                        </h6>
                                                    </div>

                                                    <div class="col-md-2 mb-3">
                                                        <h6 class="text-uppercase font-weight-bold">
                                                            <a href="cv/cv.html">CV en ligne</a>
                                                        </h6>
                                                    </div>

                                                    <div class="col-md-2 mb-3">
                                                        <h6 class="text-uppercase font-weight-bold">
                                                            <a href="projets.php">Projets</a>
                                                        </h6>
                                                    </div>

                                                    <div class="col-md-2 mb-3">
                                                        <h6 class="text-uppercase font-weight-bold">
                                                            <a href="recommandations.php">Recommandations</a>
                                                        </h6>
                                                    </div>

                                                    <div class="col-md-2 mb-3">
                                                        <h6 class="text-uppercase font-weight-bold">
                                                            <a href="https://www.linkedin.com/in/morgan-nott" target="_blank">LinkedIn</a>
                                                        </h6>
                                                    </div>

                                                    <div class="col-md-2 mb-3">
                                                        <h6 class="text-uppercase font-weight-bold">
                                                            <a href="https://github.com/Ketzatl?tab=repositories" target="_blank">GitHub</a>
                                                        </h6>
                                                    </div>






                                                </div>

                                                <hr class="rgba-white-light">






                                                </div>


                                            </div>

                                            <div class="footer-copyright text-center py-3" style="color: #EFD0EF">© 2020 Copyright
                                                <a href="https://mdbootstrap.com/education/bootstrap/"> Morgan-NOTT.fr</a>
                                            </div>

<br>
                                        </footer>









                <!-- -------------------------- Javascript ------------------------------------- -->


    <script src="https://cdnjs.cloudflare.com/ajax/libs/animejs/2.0.2/anime.min.js"></script>




                                        <script>
                                            anime.timeline({loop: true})
                                                .add({
                                                    targets: '.ml15 .word',
                                                    scale: [14,1],
                                                    opacity: [0,1],
                                                    easing: "easeOutCirc",
                                                    duration: 800,
                                                    delay: (el, i) => 800 * i
                                                }).add({
                                                targets: '.ml15',
                                                opacity: 0,
                                                duration: 1000,
                                                easing: "easeOutExpo",
                                                delay: 1000
                                            });
                                            </script>

                                        <script src="vanta-master/vendor/three.r95.min.js"></script>
                                        <script src="vanta-master/dist/vanta.fog.min.js"></script>
                                        <script>
                                            VANTA.FOG({
                                                el: "#your-element-selector",
                                                mouseControls: true,
                                                touchControls: true,
                                                minHeight: 200.00,
                                                minWidth: 200.00,
                                                highlightColor: 0x5b6570,
                                                midtoneColor: 0x520a84
                                            })
                                        </script>

<script>
    $(document).ready(function() {
        $('#cookieModal').modal('show');
    });
</script>

</body>
</html>


