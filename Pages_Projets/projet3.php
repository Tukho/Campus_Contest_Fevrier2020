<?php

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-158829501-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-158829501-1');
    </script>


    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="../css/page_projets.css">

    <title>Document</title>

</head>
<body>
<header>

    <nav class="mb-1 navbar navbar-expand-lg navbar-dark secondary-color lighten-2" style="background: #5a6268">

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-555"
                aria-controls="navbarSupportedContent-555" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent-555">

            <ul class="navbar-nav mr-auto">

                <li class="nav-item active">
                    <a class="nav-link" href="../index.php">Accueil
                        <span class="sr-only">(current)</span>
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="../documents/morgan_nott_cv.pdf" target="_blank">C.V.</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="../projets.php">Projets</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="../recommandations.php">Recommandations</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="https://www.linkedin.com/in/morgan-nott" target="_blank" >LinkedIn</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="../contact.php" target="_blank" >Contact</a>
                </li>

            </ul>

            <ul class="navbar-nav ml-auto nav-flex-icons">


                <form class="form-inline" method="GET">
                    <input class="form-control mr-sm-2" type="search" placeholder="Coming Soon..." aria-label="Search">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit" value="Valider">Valider</button>
                </form>

            </ul>


        </div>
    </nav>

</header>



<footer class="page-footer font-small black">


    <div class="footer-copyright text-center py-3" style="color: whitesmoke">Morgan NOTT 2020 ©
        <a href="morgan-nott.fr"> Morgan-NOTT.fr</a>
    </div>


</footer>


</body>
</html>

