<?php

?>

<!DOCTYPE html>

<html lang="fr">


<!-- HEAD  -->
<head>
    <meta charset="UTF-8">
    <link rel="icon" type="image/png" href="../pictures/logoMomo.ico" />
    <title>1er Projet - Morgan NOTT</title>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-158829501-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-158829501-1');
    </script>


    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <link rel="stylesheet"  href="../css/page_projets.css">



    <link href="https://fonts.googleapis.com/css?family=Paytone+One&display=swap" rel="stylesheet">



</head>


<!-- BODY  -->

<body id="your-element-selector">

<header>

    <nav class="mb-1 navbar navbar-expand-lg navbar-dark secondary-color lighten-2">

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-555"
                aria-controls="navbarSupportedContent-555" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent-555">

            <ul class="navbar-nav mr-auto">

                <li class="nav-item active">
                    <a class="nav-link" href="../index.php">Accueil
                        <span class="sr-only">(current)</span>
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="../documents/morgan_nott_cv.pdf" target="_blank">C.V.</a>
                </li>

                <li class="nav-item">
                    <a   class="nav-link" href="../cv/cv.html">CV. en ligne</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="../projets.php">Projets</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="../recommandations.php">Recommandations</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="https://www.linkedin.com/in/morgan-nott" target="_blank" >LinkedIn</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="../contact.php" target="_blank" >Contact</a>
                </li>

            </ul>

            <ul class="navbar-nav ml-auto nav-flex-icons">


                <form class="form-inline" method="GET">
                    <input class="form-control mr-sm-2" type="search" placeholder="Coming Soon..." aria-label="Search">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit" value="Valider">Valider</button>
                </form>

            </ul>


        </div>
    </nav>

</header>

<main>
    <p id="projet1">
             <h1 class="titreProjets">Création d’une Application Mobile :<br>Fil d’actualités Inter-Campus</h1>

            <br>
            <center>
            <img class="image_projets" src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/ca/AngularJS_logo.svg/695px-AngularJS_logo.svg.png" alt="angularLogo" >
            </center>

            <p>Pourquoi avons-nous choisi Angular ?<br><br>
                Il existe de nombreux frameworks JavaScript très populaires aujourd’hui : Angular, React, Ember, Vue… les autres frameworks marchent très bien, ont beaucoup de succès et sont utilisés sur des sites extrêmement bien fréquentés, React et Vue notamment. <br><br>Angular présente également un niveau de difficulté légèrement supérieur, car on utilise le TypeScript plutôt que JavaScript pur ou le mélange JS/HTML de React.</p>
            <center>
            <img src="../pictures/groupeEtudiant1.jpeg" width="500em">
            </center>
            <p>Ainsi, quels sont donc les avantages d’Angular ?<br><br>

                Angular est géré par Google — il y a donc peu de chances qu’il disparaisse, et l’équipe de développement du framework est excellente.<br><br>

                Le TypeScript — ce langage permet un développement beaucoup plus stable, rapide et facile.<br><br>

                Le framework Ionic — le framework permettant le développement d’applications mobiles multi-plateformes à partir d’une seule base de code — utilise Angular.<br><br>

                Les autres frameworks ont leurs avantages également, mais Angular est un choix très pertinent pour le développement frontend.</p>

            <section>

                <br>
                <center>
                <img  class="image_projets" class="center-block" src="https://cdn.pixabay.com/photo/2015/01/09/11/08/startup-594090_1280.jpg" alt="photoEtudiants" width="500em" align="center">
                </center>

                <p>Session de travail en groupe, dans le cadre d'un projet validant notre premier trimestre.</p>

            </section>
    </div>
</main>


<footer class="page-footer font-small black">


    <div class="footer-copyright text-center py-3" style="color: whitesmoke">Morgan NOTT 2020 ©
        <a href="morgan-nott.fr"> Morgan-NOTT.fr</a>
    </div>


</footer>

<!-- ------------------------------- Javascript ------------------------------------------- -->

<script src="../vanta-master/vendor/three.r95.min.js"></script>
<script src="../vanta-master/dist/vanta.fog.min.js"></script>
<script>
    VANTA.FOG({
        el: "#your-element-selector",
        mouseControls: true,
        touchControls: true,
        minHeight: 200.00,
        minWidth: 200.00,
        highlightColor: 0x5b6570,
        midtoneColor: 0x520a84
    })
</script>

</body>

</html>
