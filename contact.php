<?php

$bdd = new PDO('mysql:host=localhost;dbname=portfolio_cards', 'root', 'root');

if(isset($_POST['forminscription']))
{
    $prenom =htmlspecialchars($_POST['prenom']);
    $nom =htmlspecialchars($_POST['nom']);
    $entreprise =htmlspecialchars($_POST['entreprise']);
    $poste =htmlspecialchars($_POST['poste']);
    $mail =htmlspecialchars($_POST['mail']);
    $mail2 =htmlspecialchars($_POST['mail2']);
    $telephone =htmlspecialchars($_POST['telephone']);
    $message =htmlspecialchars($_POST['message']);

    if(!empty($_POST['prenom']) AND !empty($_POST['nom']) AND !empty($_POST['entreprise']) AND !empty($_POST['poste']) AND !empty($_POST['mail']) AND !empty($_POST['mail2']) AND !empty($_POST['prenom']) AND !empty($_POST['telephone']) AND !empty($_POST['message']))
    {

        $prenomlength = strlen($prenom);
        if($prenomlength <= 44)
        {
            $nomlength =strlen($nom);
            if($nomlength <= 44)
            {
                $entrepriselength =strlen($entreprise);
                if($entrepriselength <= 44)
                {
                    $postelength =strlen($poste);
                    if($postelength <= 44)
                    {
                        if($mail == $mail2)
                        {
                            if(filter_var($mail, FILTER_VALIDATE_EMAIL))
                            {
                                $insertmember = $bdd->prepare("INSERT INTO membres(prenom, nom, entreprise, poste, mail, telephone, message) VALUES (?, ?, ?, ?, ?, ?, ?)");
                                $insertmember->execute(array($prenom, $nom, $entreprise, $poste, $mail, $telephone, $message));
                                $erreur = "Merci, Votre Compte a bien été créé !";
                                header('Location: index.php');
                            }
                            else
                            {
                                $erreur = "Votre Adresse Mail n'est pas valide !";
                            }
                        }
                        else
                        {
                            $erreur = 'Vos Adresses Mail ne correspondent pas !';
                        }
                    }
                    else
                    {
                        $erreur = "L'intitulé de votre Poste doit être inférieur à 44 caractères !";
                    }
                }
                else
                {
                    $erreur = 'Le Nom de votre Entreprise doit être inférieur à 44 caractères !';
                }
            }
            else
            {
                $erreur = 'Votre nom doit être inférieur à 44 caractères !';
            }
        }
        else
        {
            $erreur = 'Votre prénom doit être inférieur à 44 caractères !';
        }

    }
    else
    {
        $erreur = 'Tous les champs doivent être renseignés !';
    }
}


?>

<!doctype html>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="icon" type="image/png" href="logos/logoMomo.ico" />

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-158829501-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-158829501-1');
    </script>


    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">


    <link rel="stylesheet" href="css/contact2.css">




    <title>Formulaire de Contact - Morgan NOTT Développeur Web & Mobile</title>

</head>
<body>

                                    <!-- -------- Navbar ------------- -->

<header style="background: #433A55">

    <nav class="mb-1 navbar navbar-expand-lg navbar-dark secondary-color lighten-2">

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-555"
                aria-controls="navbarSupportedContent-555" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent-555">

            <ul class="navbar-nav mr-auto">

                <li class="nav-item active">
                    <a class="nav-link" href="index.php">Accueil
                        <span class="sr-only">(current)</span>
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="documents/morgan_nott_cv.pdf" target="_blank">C.V.</a>
                </li>

                <li class="nav-item">
                    <a   class="nav-link" href="cv/cv.html">CV. en ligne</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="projets.php">Projets</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="recommandations.php">Recommandations</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="https://www.linkedin.com/in/morgan-nott" target="_blank" >LinkedIn</a>
                </li>

            </ul>

            <ul class="navbar-nav ml-auto nav-flex-icons">


                <form class="form-inline" method="GET">
                    <input class="form-control mr-sm-2" type="search" placeholder="Coming Soon..." aria-label="Search">
                    <button class="btn btn-outline my-2 my-sm-0" type="submit" style="color: #EFD0EF" value="Valider">Valider</button>
                </form>

            </ul>
        </div>
    </nav>

</header>

                                    <!-- ---------------------- Titre -------------------- -->
<main >
<br><br>



        <h1 class="ml15" align="center" style="color: #433A55">Me laisser une recommandation : </h1>
        <br>


        <div align="center">

            <?php
            if(isset($erreur))
            {
                echo '<font color="#f5f5f5" size="4em" > '. $erreur . "</font>";
            }
            ?>
            <br><br>



            <form method="POST" action="">

                <table>
                    <tr>
                        <th align="right">
                            <label for="prenom" >Votre Prénom :</label>
                        </th>

                        <td align="right">
                            <input id="prenom" type="text" placeholder="Votre Prénom : " name="prenom" value="<?php if(isset($prenom)) { echo $prenom;} ?>" >
                        </td>
                    </tr>

                    <tr>
                        <th align="right">
                            <label for="nom" >Votre Nom :</label>
                        </th>

                        <td align="right">
                            <input id="nom" type="text" placeholder="Votre Nom : " name="nom" value="<?php if(isset($nom)) { echo $nom;} ?>" >
                        </td>
                    </tr>

                    <tr>
                        <th align="right">
                            <label for="entreprise" >Votre Entreprise :</label>
                        </th>

                        <td align="right">
                            <input id="entreprise" type="text" placeholder="Votre Entreprise : " name="entreprise" value="<?php if(isset($entreprise)) { echo $entreprise;} ?>" >
                        </td>
                    </tr>

                    <tr>
                        <th align="right">
                            <label for="poste" >Votre Poste :</label>
                        </th>

                        <td align="right">
                            <input id="poste" type="text" placeholder="Votre Poste : " name="poste" value="<?php if(isset($poste)) { echo $poste;} ?>" >
                        </td>
                    </tr>

                    <tr>
                        <th align="right">
                            <label for="mail" >Votre Adresse Mail :</label>
                        </th>

                        <td align="right">
                            <input id="mail" type="email" placeholder="Votre Mail : " name="mail" value="<?php if(isset($mail)) { echo $mail;} ?>" >
                        </td>
                    </tr>
                    <tr>
                        <th align="right">
                            <label for="mail2" >Confirmez votre Mail :</label>
                        </th>

                        <td align="right">
                            <input id="mail2" type="email" placeholder="Confirmez votre Mail : " name="mail2">
                        </td>
                    </tr>

                    <tr>
                        <th align="right">
                            <label for="telephone" >Votre Téléphone :</label>
                        </th>

                        <td align="right">
                            <input id="telephone" type="tel" placeholder="Votre Télephone : " name="telephone" value="<?php if(isset($telephone)) { echo $telephone;} ?>" >
                        </td>
                    </tr>

                    <tr>
                        <th align="right">
                            <label for="message" >Votre Message ou Recommandation :</label>
                        </th>

                        <td align="right">
                            <input id="message" type="textarea"  placeholder="Ecrivez ici : " name="message" value="<?php if(isset($message)) { echo $message;} ?>" >
                        </td>
                    </tr>



                </table>
                <br>

                <input type="submit" name="forminscription"  class="btn btn-secondary " style="margin-bottom: 15vh" value="Envoyer !">

            </form>

        </div>

</main>

<footer class="page-footer font-small " style="background: #433A55">


    <div class="footer-copyright text-center py-3">Morgan NOTT©
        <a href="https://mdbootstrap.com/education/bootstrap/"> Morgan-NOTT.fr</a>
    </div>


</footer>

</body>

</html>

<!-- -------------------------- Javascript ------------------------------------- -->


