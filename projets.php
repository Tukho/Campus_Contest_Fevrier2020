<?php

?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-158829501-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-158829501-1');
    </script>


    <link rel="stylesheet" href="css/projets.css">

    <title>Projets Morgan NOTT - Développeur Web & Mobile</title>
</head>
<body id="your-element-selector">

<header>

    <nav class="mb-1 navbar navbar-expand-lg navbar-dark secondary-color lighten-2">

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-555"
                aria-controls="navbarSupportedContent-555" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent-555">

            <ul class="navbar-nav mr-auto">

                <li class="nav-item active">
                    <a class="nav-link" href="index.php">Accueil
                        <span class="sr-only">(current)</span>
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="documents/morgan_nott_cv.pdf" target="_blank">C.V.</a>
                </li>

                <li class="nav-item">
                    <a   class="nav-link" href="cv/cv.html">CV. en ligne</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="recommandations.php" >Recommandations</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="https://www.linkedin.com/in/morgan-nott" target="_blank">LinkedIn</a>
                </li>

            </ul>

            <ul class="navbar-nav ml-auto nav-flex-icons">


                <form class="form-inline" method="GET">
                    <input class="form-control mr-sm-2" type="search" placeholder="Coming Soon..." aria-label="Search">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit" value="Valider">Valider</button>
                </form>

            </ul>
        </div>
    </nav>

</header>

<main>
    <!-- ---------------------------- Container ---------------------------->

<div id="container_card">

    <!-- ----------------------------Elements 1 ---------------------------->

        <div class="elementsP">

            <a href="Pages_Projets/projet1.php" alt="Projet 1 Morgan NOTT - Développeur Web et Mobile" >
                <div class="elem">
                <div class="container shoe">
                    <div class="productImage elem1"></div>
                    <div class="price shoePrice">
                        <h3 style="color: whitesmoke;">Application Mobile</h3>
                        <span></span>
                    </div>

                    <div  style="font-size: 1.5em; font-weight: bold" class="productName shoeName">
                        Projet 1

                </div >
                    </div>
                    <p class="pProjets" align="center" style="width: 15em; margin-left: 18%">Création d’une Application Mobile : Fil d’actualités InterCampus, dans le cadre d’un Projet Startup Factory</p>
                </div>
            </a>
        </div>

<!-- ----------------------------Elements 2 ---------------------------->

        <div class="elementsP">

            <a href="comingsoon_13/index.html" alt="Projet 2 Morgan NOTT - Développeur Web et Mobile">
                <div class="elem">
                    <div class="container shoe">
                        <div class="productImage elem2"></div>

                        <div class="price shoePrice">
                            <h3 style="color: black;">Jeu Mobile</h3>

                        </div>
                        <div  style="font-size: 1.5em; font-weight: bold; color: black" class="productName shoeName">
                            Projet 2
                        </div>
                    </div>
                    <p class="pProjets" align="center" style="width: 15em; margin-left: 18%">Création d’un jeu Mobile : Jeu Tower Defense InterCampus, dans le cadre de notre Projet Startup Factory</p>

                </div>
            </a>
        </div>

    <!-- ----------------------------Elements 3 ---------------------------->

        <div class="elementsP">

            <a href="comingsoon_13/index.html" alt="Projet 3 Morgan NOTT - Développeur Web et Mobile">
                <div class="elem">
                    <div class="container shoe">
                        <div class="productImage elem3"></div>
                        <div class="price shoePrice">
                            <h3 style="color: orangered; font-size: ">Reseau Social</h3>

                        </div>
                        <div  style="font-size: 1.5em; color: orangered; font-weight: bold" class="productName shoeName">
                            Projet 3
                        </div>
                    </div>
                    <p class="pProjets" align="center" >Création d’un réseau social sur Mobile & Web, dans le
                        cadre de notre Projet Startup Factory</p>
                </div>

            </a>
        </div>
<!-- </div> -->
        <!-- ---------------------------- Container ---------------------------->

<!--  <div id="container_card2">  -->

        <!-- ----------------------------Elements 4 ---------------------------->

        <div class="elementsP">

            <a href="comingsoon_13/index.html" alt="Projet  Morgan NOTT - Développeur Web et Mobile">
                <div class="elem">
                    <div class="container shoe">
                        <div class="productImage elem3"></div>
                        <div class="price shoePrice">
                            <h3 style="color: whitesmoke; font-size: ">Stage 1</h3>

                        </div>
                        <div style="font-size: 1.5em"  class="productName shoeName">
                            Projet 4
                        </div>
                    </div>
                </div>
            </a>
        </div>

        <!-- ----------------------------Elements 5 ---------------------------->

        <div class="elementsP">

             <a href="comingsoon_13/index.html" alt="Projet  Morgan NOTT - Développeur Web et Mobile">
                    <div class="elem">
                        <div class="container shoe">
                            <div class="productImage elem3"></div>
                            <div class="price shoePrice">
                                <h3 style="color: whitesmoke; font-size: ">Stage 2</h3>

                            </div>
                            <div style="font-size: 1.5em" class="productName shoeName">
                                Projet 5
                            </div>
                        </div>
                    </div>
             </a>
        </div>

            <!-- ----------------------------Elements 6 ---------------------------->

        <div class="elementsP">

            <a href="comingsoon_13/index.html" alt="Projet  Morgan NOTT - Développeur Web et Mobile">
                    <div class="elem">
                        <div class="container shoe">
                            <div class="productImage elem3"></div>
                            <div class="price shoePrice">
                                <h3 style="color: whitesmoke; font-size: ">Projet 6</h3>

                            </div>
                            <div style="font-size: 1.5em"  class="productName shoeName">
                                Projet 6
                            </div>
                        </div>
                    </div>
            </a>
        </div>
</div>
            <!-- -------------------------------------------------------->



    <!-- -------------------------------------------------------->

</main>
<!-- Footer -->
<footer class="page-footer font-small blue-grey">


    <div class="footer-copyright text-center py-3" style="color: #EFD0EF">Morgan NOTT 2020 ©
        <a href="https://mdbootstrap.com/education/bootstrap/" style="color: #EFD0EF"> Morgan-NOTT.fr</a>
    </div>


</footer>
<!-- Footer -->

<script src="vanta-master/vendor/three.r95.min.js"></script>
<script src="vanta-master/dist/vanta.fog.min.js"></script>
<script>
    VANTA.FOG({
        el: "#your-element-selector",
        mouseControls: true,
        touchControls: true,
        minHeight: 200.00,
        minWidth: 200.00,
        highlightColor: 0x5b6570,
        midtoneColor: 0x520a84
    })
</script>

</body>
</html>
